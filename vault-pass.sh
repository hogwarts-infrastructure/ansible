#!/bin/bash

# Variables

GPG_ENCRYPTED_VAULT_PASSWORD_FILE="${HOME}/.ansible.hogwarts.vault-pass.gpg"
if [[ -f "${GPG_ENCRYPTED_VAULT_PASSWORD_FILE}" ]]; then
  if gpg --list-packets "${GPG_ENCRYPTED_VAULT_PASSWORD_FILE}" > /dev/null\
  2>&1 ; then
    gpg --batch \
        --quiet \
        --no-tty \
        --for-your-eyes-only \
        --decrypt \
        "${GPG_ENCRYPTED_VAULT_PASSWORD_FILE}"
  else
    echo "${GPG_ENCRYPTED_VAULT_PASSWORD_FILE} does not seem to be GPG" >&2
    echo "encrypted" >&2
    exit 2
  fi
else
  echo "${GPG_ENCRYPTED_VAULT_PASSWORD_FILE} does not exist" >&2
  exit 1
fi
